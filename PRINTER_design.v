`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: zyf
// 
// Create Date: 2018/03/08 14:05:57
// Design Name: 
// Module Name: printer
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module PRINTER_design(printer_RDY,printer_TR,printer_PD,CLK);
input printer_TR;
input CLK;
input[7:0] printer_PD;
output printer_RDY;
reg [7:0] data_in;
reg printer_RDY;

initial
begin
printer_RDY=1;
data_in=8'b0000_0000;
end

always@(posedge CLK)
begin
    if(printer_TR==1&&printer_RDY==1)
    begin
	printer_RDY=0;
	end
	if(printer_TR==1&&printer_RDY==0)
	begin
	data_in <= printer_PD;
    #500 printer_RDY <= 1;
	end    
end


endmodule

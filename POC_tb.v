`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2018/03/14 10:27:40
// Design Name: 
// Module Name: POC_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module POC_tb();

reg RST;
reg CLK;
reg RW;
reg [1:0]ADDR;
reg [7:0]Din;
reg MODE;

 POC_design tst(
				.RST(RST),
				.CLK(CLK),
				.RW(RW),
				.ADDR(ADDR),
				.Din(Din),
				.MODE(MODE)
			   );
    
initial
begin
fork
	 RST <= 1'b0;
	 CLK <= 1'b0;
	 MODE = 1'b0;
	 ADDR = 2'b10;
	 Din = 8'b1000_0000;
	 RW = 1'b1; 
join

	#1000 RST = 1'b1;
	#(10000*1000) $stop;
end

always 
  #50 CLK = ~CLK;


always@(posedge CLK)
begin
	assign MODE = 1'b0;
	assign ADDR = 2'b10;
	assign RW = 1'b0;
end
endmodule
